/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ris_implementacija_jan_lukac;

/**
 *
 * @author Uporabnik
 */
public class Pes {
    private String ime;
    private String pasma;
    private int letoRojstva;
    private String spol;
    private boolean cipiran;
    private String opis;

    public Pes(String ime, String pasma, int letoRojstva, String spol, boolean cipiran, String opis) {
        this.ime = ime;
        this.pasma = pasma;
        this.letoRojstva = letoRojstva;
        this.spol = spol;
        this.cipiran = cipiran;
        this.opis = opis;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public boolean isCipiran() {
        return cipiran;
    }

    public void setCipiran(boolean cipiran) {
        this.cipiran = cipiran;
    }

    public String getPasma() {
        return pasma;
    }

    public int getLetoRojstva() {
        return letoRojstva;
    }

    public String getSpol() {
        return spol;
    }

    public String getOpis() {
        return opis;
    }

    @Override
    public String toString() {
        return "Pes{" + "ime=" + ime + ", pasma=" + pasma + ", letoRojstva=" + letoRojstva + ", spol=" + spol + ", cipiran=" + cipiran + ", opis=" + opis + '}';
    }
    
    public String izpis() {
        String cip;
        if (cipiran) {
            cip = "je";
        } else {
            cip = "ni";
        }
        return "Mojemu psu je ime" + " " + ime + ", je pasme " + pasma + ". Rojen je leta" + letoRojstva + ", je spola " + spol + ", " + cip + " cipiran in moj opis o njem je: " + opis + '.';
    }
    
    
}
